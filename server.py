from flask import Flask
from flask import render_template, request, redirect, url_for, flash

app = Flask(__name__,template_folder='../Templates')

@app.route("/")
def hello_world():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)